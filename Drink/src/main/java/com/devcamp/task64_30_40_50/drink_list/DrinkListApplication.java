package com.devcamp.task64_30_40_50.drink_list;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DrinkListApplication {

	public static void main(String[] args) {
		SpringApplication.run(DrinkListApplication.class, args);
	}

}
