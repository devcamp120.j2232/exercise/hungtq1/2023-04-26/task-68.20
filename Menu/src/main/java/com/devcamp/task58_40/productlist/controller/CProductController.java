package com.devcamp.task58_40.productlist.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task58_40.productlist.model.product;
import com.devcamp.task58_40.productlist.repository.ProductRepository;

@RequestMapping("/")
@CrossOrigin
@RestController
public class CProductController {
    @Autowired
    ProductRepository productRepository;

    @GetMapping("/products")
    public ResponseEntity<List<product>> getAllProducts(){
        try {
            List<product> listProduct = new ArrayList<product>();
            productRepository.findAll().forEach(listProduct::add);
            return new ResponseEntity<>(listProduct, HttpStatus.OK);

        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
